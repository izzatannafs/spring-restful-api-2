show databases ;

CREATE DATABASE wms_api;

USE wms_api;

CREATE TABLE branch(
        branch_id VARCHAR (100) NOT NULL ,
        branch_name VARCHAR (100) NOT NULL ,
        branch_code VARCHAR (100) NOT NULL ,
        address TEXT  ,
        phone_number VARCHAR(100) NOT NULL,
        PRIMARY KEY (branch_id)
) ENGINE InnoDB;

SELECT * FROM branch;

DESC branch;

CREATE TABLE products(
        product_id VARCHAR (100) NOT NULL ,
        product_price_id VARCHAR (100) ,
        product_code VARCHAR (100) NOT NULL ,
        product_name VARCHAR (100) NOT NULL ,
        price BIGINT NOT NULL NOT NULL ,
        branch_id VARCHAR(100),
        PRIMARY KEY (product_id),
        FOREIGN KEY (branch_id) REFERENCES branch(branch_id)
) ENGINE InnoDB;

SELECT * FROM products;

DESC products;

CREATE TABLE transactions(
        bill_id VARCHAR(100) NOT NULL ,
        receipt_number VARCHAR(100) NOT NULL ,
        trans_date VARCHAR(100) NOT NULL ,
        transaction_type VARCHAR(100) NOT NULL ,
        product_id VARCHAR(100),
        PRIMARY KEY (bill_id),
        FOREIGN KEY (product_id) REFERENCES products(product_id)
) ENGINE InnoDB;

SELECT * FROM transactions;

DESC transactions;

CREATE TABLE transaction_details(
        bill_detail_id VARCHAR(100) NOT NULL ,
        bill_detail VARCHAR(100) NOT NULL ,
        quantity INT NOT NULL ,
        total_sales BIGINT NOT NULL ,
        bill_id VARCHAR(100),
        product_id VARCHAR(100),
        PRIMARY KEY (bill_detail_id) ,
        FOREIGN KEY (bill_id) REFERENCES transactions (bill_id),
        FOREIGN KEY (product_id) REFERENCES products(product_id)

) ENGINE InnoDB;

SELECT * FROM transaction_details;

DESC transaction_details;

DROP TABLE transaction_details;