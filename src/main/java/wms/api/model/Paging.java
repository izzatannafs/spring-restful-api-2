package wms.api.model;

import lombok.*;

@Getter
@Setter
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Paging {

    private int totalItems;
    private int totalPages;
    private int page;
    private int size;
    
}
