package wms.api.model;

import lombok.*;

import java.math.BigDecimal;

@Setter
@Getter
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductResponse {

    private String productId;
    private String productPriceId;
    private String productCode;
    private String productName;
    private BigDecimal price;
    private BranchResponse branch;

    @Setter
    @Getter
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    public static class BranchResponse  {
        private String branchId;
        private String branchCode;
        private String branchName;
        private String address;
        private String phoneNumber;

    }
}
