package wms.api.model;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdateProductRequest {

    @NotBlank
    @Size(max = 100)
    private String productId;

    @NotBlank
    @Size(max = 100)
    private String  productCode;

    @NotBlank
    @Size(max = 100)
    private String productName;

    @NotNull
    @Min(value = 1)
    private BigDecimal price;

    @NotNull
    @Size(max = 100)
    private String productPriceId;

    @NotBlank
    @Size(max = 100)
    private String branchId;
}
