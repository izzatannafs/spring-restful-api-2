package wms.api.model;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateTransactionRequest {

    @NotBlank
    @Size(max = 10)
    private String transactionType;

    @NotNull
    @Valid
    private List<BillDetailRequest> billDetail;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    public static class BillDetailRequest {

        @NotBlank
        private String productPriceId;

        @NotNull
        @Min(1)
        private Integer quantity;
    }
}
