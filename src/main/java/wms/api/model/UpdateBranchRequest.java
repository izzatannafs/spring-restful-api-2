package wms.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdateBranchRequest {

    @NotBlank
    private String branchId;

    @NotBlank
    @Size(max = 100)
    private String branchCode;

    @NotBlank
    @Size(max = 100)
    private String branchName;

    @NotBlank
    private String address;

    @NotBlank
    @Size(max = 100)
    private String phoneNumber;
}
