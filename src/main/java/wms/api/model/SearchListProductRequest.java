package wms.api.model;

import jakarta.validation.constraints.NotNull;
import lombok.*;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SearchListProductRequest {

    @NotNull
    private Integer size;

    @NotNull
    private Integer page;

    private String productCode;

    private String productName;

    private String minPrice;

    private String maxPrice;
}
