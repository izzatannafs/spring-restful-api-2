package wms.api.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransactionResponse {

    private String billId;
    private String receiptNumber;
    private String transDate;
    private String transactionType;
    private List<BillDetailResponse> billDetails;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    public static class BillDetailResponse {

        private String billDetailId;
        private String billId;
        private ProductResponse product;
        private Integer quantity;
        private BigDecimal totalSales;

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        @Builder
        public static class ProductResponse {
            private String productId;
            private String productPriceId;
            private String productCode;
            private String productName;
            private BigDecimal price;
            private BranchResponse branch;

            @Data
            @AllArgsConstructor
            @NoArgsConstructor
            @Builder
            public static class BranchResponse {
                private String branchId;
                private String branchCode;
                private String branchName;
                private String address;
                private String phoneNumber;
            }
        }
    }
}
