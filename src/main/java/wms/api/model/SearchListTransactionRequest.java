package wms.api.model;

import jakarta.validation.constraints.NotNull;
import lombok.*;

@Getter
@Setter
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SearchListTransactionRequest {

    @NotNull
    private Integer page;

    @NotNull
    private Integer size;

    private String receiptNumber;

    private String startDate;

    private String endDate;

    private String transType;

    private String productName;
}
