package wms.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;
import wms.api.entity.Branch;
import wms.api.model.BranchResponse;
import wms.api.model.CreateBranchRequest;
import wms.api.model.UpdateBranchRequest;
import wms.api.repository.BranchRepository;

import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Service
public class BranchService {

    @Autowired
    private BranchRepository branchRepository;

    @Autowired
    private ValidationService validationService;

    @Transactional
    public BranchResponse create(CreateBranchRequest request) {
        validationService.validate(request);

        Branch branch = new Branch();
        branch.setBranchId(UUID.randomUUID().toString());
        branch.setBranchName(request.getBranchName());
        branch.setBranchCode(request.getBranchCode());
        branch.setAddress(request.getAddress());
        branch.setPhoneNumber(request.getPhoneNumber());
        branchRepository.save(branch);

        return toBranchResponse(branch);
    }

    @Transactional(readOnly = true)
    public Optional<BranchResponse> getBranchById(String idBranch) {
        Optional<Branch> optionalBranch = branchRepository.findById(idBranch);
        if (optionalBranch.isPresent()) {
            Branch branch = optionalBranch.get();
            BranchResponse branchResponse = toBranchResponse(branch);
            return Optional.of(branchResponse);
        } else {
            return Optional.empty();
        }
    }

    @Transactional
    public BranchResponse update(Branch branch, UpdateBranchRequest request) {
        validationService.validate(request);

        if (Objects.nonNull(request.getBranchId())) {
            branch.setBranchId(request.getBranchId());
        }

        if (Objects.nonNull(request.getBranchName())) {
            branch.setBranchName(request.getBranchName());
        }

        if (Objects.nonNull(request.getBranchCode())) {
            branch.setBranchCode(request.getBranchCode());
        }

        if (Objects.nonNull(request.getAddress())) {
            branch.setAddress(request.getAddress());
        }

        if (Objects.nonNull(request.getPhoneNumber())) {
            branch.setPhoneNumber(request.getPhoneNumber());
        }

        branchRepository.save(branch);

        return toBranchResponse(branch);
    }

    @Transactional
    public void delete(String idBranch) {
        Branch branch = branchRepository.findById(idBranch)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Branch not found"));
        branchRepository.delete(branch);
    }

    private BranchResponse toBranchResponse(Branch branch) {
        return BranchResponse.builder()
                .branchId(branch.getBranchId())
                .branchName(branch.getBranchName())
                .branchCode(branch.getBranchCode())
                .address(branch.getAddress())
                .phoneNumber(branch.getPhoneNumber())
                .build();
    }
}
