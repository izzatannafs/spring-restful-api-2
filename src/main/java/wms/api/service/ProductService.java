package wms.api.service;

import jakarta.persistence.criteria.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;
import wms.api.entity.Branch;
import wms.api.entity.Product;
import wms.api.model.*;
import wms.api.repository.ProductRepository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ValidationService validationService;

    @Transactional
    public ProductResponse create(Branch branch, CreateProductRequest request) {
        validationService.validate(request);

        Product product = new Product();
        product.setProductId(UUID.randomUUID().toString());
        product.setProductName(request.getProductName());
        product.setProductCode(request.getProductCode());
        product.setProductPriceId(UUID.randomUUID().toString());
        product.setPrice(request.getPrice());
        product.setBranch(branch);
        productRepository.save(product);

        return toProductResponse(product, branch);
    }

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Transactional(readOnly = true)
    public Page<ProductResponse> search(Branch branch, SearchListProductRequest request) {
        Specification<Product> specification = (root, query, builder) -> {
            List<jakarta.persistence.criteria.Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get("branch"), branch));

            if (Objects.nonNull(request.getProductCode())) {
                predicates.add(builder.like(root.get("productCode"), "%" + request.getProductCode() + "%"));
            }
            if (Objects.nonNull(request.getProductName())) {
                predicates.add(builder.like(root.get("productName"), "%" + request.getProductName() + "%"));
            }
            if (Objects.nonNull(request.getMaxPrice())) {
                try {
                    BigDecimal maxPrice = new BigDecimal(request.getMaxPrice());
                    predicates.add(builder.lessThanOrEqualTo(root.get("price"), maxPrice));
                } catch (NumberFormatException e) {
                    // handle exception or log it
                }
            }
            if (Objects.nonNull(request.getMinPrice())) {
                try {
                    BigDecimal minPrice = new BigDecimal(request.getMinPrice());
                    predicates.add(builder.greaterThanOrEqualTo(root.get("price"), minPrice));
                } catch (NumberFormatException e) {
                    // handle exception or log it
                }
            }

            return builder.and(predicates.toArray(new Predicate[0]));
        };

        Pageable pageable = PageRequest.of(request.getPage(), request.getSize());
        Page<Product> products = productRepository.findAll(specification, pageable);
        List<ProductResponse> productResponses = products.getContent().stream()
                //.map(this::toProductResponse)
                .map(product -> toProductResponse(product, branch))
                .toList();

        return new PageImpl<>(productResponses, pageable, products.getTotalElements());
    }

    @Transactional(readOnly = true)
    public ProductResponse get(Branch branch, String id) {
        Product product = productRepository.findByBranchAndProductId(branch, id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Product Not Found"));
        return toProductResponse(product, branch);
    }

    @Transactional
    public ProductResponse update(Branch branch, UpdateProductRequest request) {
        validationService.validate(request);

        Product product = productRepository.findByBranchAndProductId(branch, request.getProductId())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Product Not Found"));

        product.setProductId(request.getProductId());
        product.setProductName(request.getProductName());
        product.setProductCode(request.getProductCode());
        product.setPrice(request.getPrice());
        product.setProductPriceId(request.getProductPriceId());
        product.setBranch(branch);

        productRepository.save(product);

        return toProductResponse(product, branch);

    }

    @Transactional
    public void delete(String productId){
        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Product Not Found"));
        productRepository.delete(product);
    }

    private ProductResponse toProductResponse(Product product, Branch branch) {
        ProductResponse.BranchResponse branchResponse = ProductResponse.BranchResponse.builder()
                .branchId(branch.getBranchId())
                .branchCode(branch.getBranchCode())
                .branchName(branch.getBranchName())
                .address(branch.getAddress())
                .phoneNumber(branch.getPhoneNumber())
                .build();

        return ProductResponse.builder()
                .productId(product.getProductId())
                .productCode(product.getProductCode())
                .productName(product.getProductName())
                .price(product.getPrice())
                .productPriceId(product.getProductPriceId())
                .branch(branchResponse)
                .build();


//        ProductResponse response = new ProductResponse();
//        response.setProductId(product.getProductId());
//        response.setProductName(product.getProductName());
//        response.setProductCode(product.getProductCode());
//        response.setProductPriceId(product.getProductPriceId());
//        response.setPrice(product.getPrice());
//
//        ProductResponse.BranchResponse branchResponse = new ProductResponse.BranchResponse();
//        branchResponse.setBranchId(product.getBranch().getBranchId());
//        branchResponse.setBranchCode(product.getBranch().getBranchCode());
//        branchResponse.setBranchName(product.getBranch().getBranchName());
//        branchResponse.setAddress(product.getBranch().getAddress());
//        branchResponse.setPhoneNumber(product.getBranch().getPhoneNumber());
//
//        response.setBranch(branchResponse);
//
//        return response;
    }
}
