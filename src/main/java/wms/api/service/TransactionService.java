package wms.api.service;

import jakarta.persistence.criteria.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;
import wms.api.entity.Branch;
import wms.api.entity.Product;
import wms.api.entity.TransactionDetails;
import wms.api.entity.Transactions;
import wms.api.model.CreateTransactionRequest;
import wms.api.model.SearchListTransactionRequest;
import wms.api.model.TransactionResponse;
import wms.api.model.WebResponse;
import wms.api.repository.BranchRepository;
import wms.api.repository.ProductRepository;
import wms.api.repository.TransactionDetailsRepository;
import wms.api.repository.TransactionRepository;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class TransactionService {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private TransactionDetailsRepository transactionDetailsRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private BranchRepository branchRepository;

    @Autowired
    private ValidationService validationService;

    @Transactional
    public TransactionResponse create(CreateTransactionRequest request) {
        validationService.validate(request);

        Transactions transactions = new Transactions();
        transactions.setTransactionType(request.getTransactionType());
        transactionRepository.save(transactions);

        List<TransactionResponse.BillDetailResponse> billDetails = new ArrayList<>();
        for (CreateTransactionRequest.BillDetailRequest billDetailRequest : request.getBillDetail()) {
            Product product = productRepository.findByProductPriceId(billDetailRequest.getProductPriceId())
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Product Not Found"));

            Branch branch = product.getBranch();

            TransactionDetails transactionDetails = new TransactionDetails();
            transactionDetails.setTransactions(transactions);
            transactionDetails.setProduct(product);
            transactionDetails.setQuantity(billDetailRequest.getQuantity());
            transactionDetails.setTotalSales(product.getPrice().multiply(BigDecimal.valueOf(billDetailRequest.getQuantity())));
            transactionDetailsRepository.save(transactionDetails);

            TransactionResponse.BillDetailResponse.ProductResponse.BranchResponse branchResponse = TransactionResponse.BillDetailResponse.ProductResponse.BranchResponse.builder()
                    .branchId(product.getBranch().getBranchId())
                    .branchCode(product.getBranch().getBranchCode())
                    .branchName(product.getBranch().getBranchName())
                    .address(product.getBranch().getAddress())
                    .phoneNumber(product.getBranch().getPhoneNumber())
                    .build();

            TransactionResponse.BillDetailResponse.ProductResponse productResponse = TransactionResponse.BillDetailResponse.ProductResponse.builder()
                    .productId(product.getProductId())
                    .productPriceId(product.getProductPriceId())
                    .productCode(product.getProductCode())
                    .productName(product.getProductName())
                    .price(product.getPrice())
                    .branch(branchResponse)
                    .build();

            TransactionResponse.BillDetailResponse billDetailResponse = TransactionResponse.BillDetailResponse.builder()
                    .billDetailId(transactionDetails.getBillDetailId())
                    .billId(transactions.getBillId())
                    .product(productResponse)
                    .quantity(transactionDetails.getQuantity())
                    .totalSales(transactionDetails.getTotalSales())
                    .build();

            billDetails.add(billDetailResponse);
        }

        String transactionTypeStr;
        switch (transactions.getTransactionType()) {
            case "1":
                transactionTypeStr = "EAT_IN";
                break;
            case "2":
                transactionTypeStr = "ONLINE";
                break;
            case "3":
                transactionTypeStr = "TAKE_AWAY";
                break;
            default:
                transactionTypeStr = "UNKNOWN";
        }

        // Format receipt number
        String branchCode = billDetails.get(0).getProduct().getBranch().getBranchCode();
        String receiptNumber = branchCode + "_" + LocalDateTime.now().getYear() + "_" + transactions.getReceiptNumber();

        // Format transDate
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        String transDateFormatted = LocalDateTime.now().format(formatter);

        // Buat respon transaksi
        return TransactionResponse.builder()
                .billId(transactions.getBillId())
                .receiptNumber(receiptNumber)
                .transDate(transDateFormatted)
                .transactionType(transactionTypeStr)
                .billDetails(billDetails)
                .build();
    }

    @Transactional
    public TransactionResponse get(String billId) {
        Transactions transactions = transactionRepository.findByBillId(billId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Transaction Not Found"));

        System.out.println("Transaction found: " + transactions);

        List<TransactionDetails> transactionDetailsList = transactionDetailsRepository.findAllByTransactions(transactions);

        List<TransactionResponse.BillDetailResponse> billDetails = new ArrayList<>();
        for (TransactionDetails transactionDetails : transactionDetailsList) {
            Product product = transactionDetails.getProduct();
            Branch branch = product.getBranch();

            TransactionResponse.BillDetailResponse.ProductResponse.BranchResponse branchResponse = TransactionResponse.BillDetailResponse.ProductResponse.BranchResponse.builder()
                    .branchId(product.getBranch().getBranchId())
                    .branchCode(product.getBranch().getBranchCode())
                    .branchName(product.getBranch().getBranchName())
                    .address(product.getBranch().getAddress())
                    .phoneNumber(product.getBranch().getPhoneNumber())
                    .build();

            TransactionResponse.BillDetailResponse.ProductResponse productResponse = TransactionResponse.BillDetailResponse.ProductResponse.builder()
                    .productId(product.getProductId())
                    .productPriceId(product.getProductPriceId())
                    .productCode(product.getProductCode())
                    .productName(product.getProductName())
                    .price(product.getPrice())
                    .branch(branchResponse)
                    .build();

            TransactionResponse.BillDetailResponse billDetailResponse = TransactionResponse.BillDetailResponse.builder()
                    .billDetailId(transactionDetails.getBillDetailId())
                    .billId(transactions.getBillId())
                    .product(productResponse)
                    .quantity(transactionDetails.getQuantity())
                    .totalSales(transactionDetails.getTotalSales())
                    .build();

            billDetails.add(billDetailResponse);
        }

        String transactionTypeStr;
        switch (transactions.getTransactionType()) {
            case "1":
                transactionTypeStr = "EAT_IN";
                break;
            case "2":
                transactionTypeStr = "ONLINE";
                break;
            case "3":
                transactionTypeStr = "TAKE_AWAY";
                break;
            default:
                transactionTypeStr = "UNKNOWN";
        }

        // Format receipt number
        String branchCode = billDetails.get(0).getProduct().getBranch().getBranchCode();
        String receiptNumber = branchCode + "_" + LocalDateTime.now().getYear() + "_" + transactions.getReceiptNumber();

        // Format transDate
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        String transDateFormatted = LocalDateTime.now().format(formatter);

        // Buat respon transaksi
        return TransactionResponse.builder()
                .billId(transactions.getBillId())
                .receiptNumber(receiptNumber)
                .transDate(transDateFormatted)
                .transactionType(transactionTypeStr)
                .billDetails(billDetails)
                .build();
    }

}
