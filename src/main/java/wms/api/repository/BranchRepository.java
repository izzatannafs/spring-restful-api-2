package wms.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import wms.api.entity.Branch;

import java.util.Optional;

@Repository
public interface BranchRepository extends JpaRepository<Branch, String > {
    @Query("SELECT b FROM Branch b JOIN FETCH b.products WHERE b.branchId = :branchId")
    Optional<Branch> findByBranchIdWithProducts(@Param("branchId") String branchId);

}
