package wms.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import wms.api.entity.Product;
import wms.api.entity.Transactions;

import java.util.Optional;

@Repository
public interface TransactionRepository extends JpaRepository<Transactions, String >, JpaSpecificationExecutor<Transactions> {
    Optional<Transactions> findByBillId(String billId);
}
