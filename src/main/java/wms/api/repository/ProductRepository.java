package wms.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import wms.api.entity.Branch;
import wms.api.entity.Product;

import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product, String >, JpaSpecificationExecutor<Product> {
    Optional<Product> findByBranchAndProductId(Branch branch, String productId);

    Optional<Product> findByProductPriceId(String productPriceId);
}
