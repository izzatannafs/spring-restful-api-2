package wms.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import wms.api.entity.TransactionDetails;
import wms.api.entity.Transactions;

import java.util.List;

@Repository
public interface TransactionDetailsRepository extends JpaRepository<TransactionDetails, String > {
    List<TransactionDetails> findAllByTransactions(Transactions transactions);
}
