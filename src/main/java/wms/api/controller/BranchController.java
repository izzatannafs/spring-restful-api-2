package wms.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import wms.api.entity.Branch;
import wms.api.model.*;
import wms.api.repository.BranchRepository;
import wms.api.service.BranchService;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;

@RestController
public class BranchController {

    @Autowired
    private BranchRepository branchRepository;
    @Autowired
    private BranchService branchService;

    @PostMapping(
            path = "/api/branch",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<WebResponse<BranchResponse>> create(@RequestBody CreateBranchRequest request) {
        BranchResponse branchResponse = BranchResponse.builder()
                .branchId(UUID.randomUUID().toString())
                .branchCode(request.getBranchCode())
                .branchName(request.getBranchName())
                .address(request.getAddress())
                .phoneNumber(request.getPhoneNumber())
                .build();
        WebResponse<BranchResponse> response = new WebResponse<>(branchResponse, null, null);
        return ResponseEntity.ok(response);
    }

//    @GetMapping(
//            path = "/api/branch/{id_branch}",
//            produces = MediaType.APPLICATION_JSON_VALUE
//    )
//    public ResponseEntity<WebResponse<BranchResponse>> get(@PathVariable("id_branch") String idBranch) {
//        Optional<BranchResponse> optionalBranchResponse = branchService.getBranchById(idBranch);
//        if (optionalBranchResponse.isPresent()) {
//            BranchResponse branchResponse = optionalBranchResponse.get();
//            Paging paging = new Paging(0, 10, 1, 1);
//
//            WebResponse<BranchResponse> response = new WebResponse<>(branchResponse, null, paging);
//            return ResponseEntity.ok(response);
//        } else {
//            WebResponse<BranchResponse> response = new WebResponse<>(null, "Branch not found : " + idBranch, null);
//            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
//        }
//
//    }

    @GetMapping(
            path = "/api/branch/{id_branch}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public WebResponse<BranchResponse> getBranch(@PathVariable("id_branch") String idBranch) {
        Branch branch = branchRepository.findById(idBranch)
                .orElseThrow(() -> new NoSuchElementException("Branch Not Found"));

        BranchResponse branchResponse = toBranchResponse(branch);
        return WebResponse.<BranchResponse>builder()
                .data(branchResponse)
                .paging(new Paging(1, 1, 0, 10)) // Pastikan nilai paging diatur dengan benar
                .build();
    }

    private BranchResponse toBranchResponse(Branch branch) {
        BranchResponse response = new BranchResponse();
        response.setBranchId(branch.getBranchId());
        response.setBranchName(branch.getBranchName());
        response.setBranchCode(branch.getBranchCode());
        response.setAddress(branch.getAddress());
        response.setPhoneNumber(branch.getPhoneNumber());
        return response;
    }

    @PutMapping(
            path = "/api/branch",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public WebResponse<BranchResponse> update(Branch branch, @RequestBody UpdateBranchRequest request) {
        BranchResponse branchResponse = branchService.update(branch, request);
        return WebResponse.<BranchResponse>builder().data(branchResponse).build();
    }

    @DeleteMapping(
            path = "/api/branch/{id_branch}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public WebResponse<String> delete(@PathVariable("id_branch") String idBranch) {
        branchService.delete(idBranch);
        return WebResponse.<String>builder().data("Ok").build();
    }
}
