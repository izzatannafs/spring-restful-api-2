package wms.api.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import wms.api.entity.Transactions;
import wms.api.model.CreateTransactionRequest;
import wms.api.model.TransactionResponse;
import wms.api.model.WebResponse;
import wms.api.repository.TransactionRepository;
import wms.api.service.TransactionService;

@RestController
public class TransactionController {

    private final TransactionService transactionService;

    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @PostMapping(
            path = "/api/transactions",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public WebResponse<TransactionResponse> create(@RequestBody CreateTransactionRequest request){
        TransactionResponse transactionResponse = transactionService.create(request);
        return WebResponse.<TransactionResponse>builder().data(transactionResponse).build();
    }

    @GetMapping(
            path = "/api/transaction/{id_bill}",
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public WebResponse<TransactionResponse> get(@PathVariable("id_bill") String idBill) {
        TransactionResponse transactionResponse = transactionService.get(idBill);
        return WebResponse.<TransactionResponse>builder()
                .data(transactionResponse)
                .errors(null)
                .paging(null)
                .build();
    }

//    @GetMapping(
//            path = "/api/transaction/{id_bill}",
//            consumes = MediaType.APPLICATION_JSON_VALUE
//    )
//    public WebResponse<TransactionResponse> get(@PathVariable("id_bill") String idBill) {
//        TransactionResponse transactionResponse = transactionService.get(idBill);
//        return WebResponse.<TransactionResponse>builder()
//                .data(transactionResponse)
//                .errors(null)
//                .paging(null)
//                .build();
//    }
}
