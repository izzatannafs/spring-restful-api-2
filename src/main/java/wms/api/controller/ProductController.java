package wms.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import wms.api.entity.Branch;
import wms.api.model.*;
import wms.api.repository.BranchRepository;
import wms.api.service.ProductService;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
public class ProductController {

    @Autowired
    private ProductService productService;

    @Autowired
    private BranchRepository branchRepository;

    @PostMapping(
            path = "/api/products",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public WebResponse<ProductResponse> create(@RequestBody CreateProductRequest request) {
        Branch branch = branchRepository.findById(request.getBranchId())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Branch not Found"));

        ProductResponse productResponse = productService.create(branch, request);
        return WebResponse.<ProductResponse>builder().data(productResponse).build();
    }

    @GetMapping(
            path = "/api/products",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public WebResponse<List<ProductResponse>> search(
                                                     @RequestParam(value = "branchId") String branchId,
                                                     @RequestParam(value = "productName", required = false) String productName,
                                                     @RequestParam(value = "productCode", required = false) String productCode,
                                                     @RequestParam(value = "minPrice", required = false) String minPrice,
                                                     @RequestParam(value = "maxPrice", required = false) String maxPrice,
                                                     @RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
                                                     @RequestParam(value = "size", required = false, defaultValue = "10") Integer size) {
        Branch branch = branchRepository.findById(branchId)
                .orElseThrow(() -> new NoSuchElementException("Branch not Found"));

        SearchListProductRequest request = SearchListProductRequest.builder()
                .productName(productName)
                .productCode(productCode)
                .maxPrice(maxPrice)
                .minPrice(minPrice)
                .page(page)
                .size(size)
                .build();
        Page<ProductResponse> productResponses = productService.search(branch, request);

        Paging paging = new Paging(
                productResponses.getNumberOfElements(),
                productResponses.getTotalPages(),
                productResponses.getNumber(),
                productResponses.getSize()
        );

        return WebResponse.<List<ProductResponse>>builder()
                .data(productResponses.getContent())
                .paging(paging)
                .build();
    }

    @GetMapping(
            path = "/api/products/{id_branch}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public WebResponse<ProductResponse> get(
            @PathVariable("id_branch") String idBranch,
            @RequestParam("productId") String productId) {
        Branch branch = branchRepository.findById(idBranch)
                .orElseThrow(() -> new NoSuchElementException("Branch Not Found"));

        ProductResponse productResponse = productService.get(branch, productId);

        List<ProductResponse> productList = new ArrayList<>();
        productList.add(productResponse);

        return WebResponse.<ProductResponse>builder()
                .data(productResponse)
                .paging(new Paging(1, 1, 0, 1))
                .build();
    }

    @PutMapping(
            path = "/api/products",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public WebResponse<ProductResponse> update(@RequestBody UpdateProductRequest request) {
        Branch branch = branchRepository.findById(request.getBranchId())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Branch Not Found"));

        ProductResponse productResponse = productService.update(branch, request);
        return WebResponse.<ProductResponse>builder().data(productResponse).build();
    }

    @DeleteMapping(
            path = "/api/products/{id_product}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public WebResponse<String> delete(@PathVariable("id_product") String id_product) {
        productService.delete(id_product);
        return WebResponse.<String>builder().data("OK").build();
    }
}
