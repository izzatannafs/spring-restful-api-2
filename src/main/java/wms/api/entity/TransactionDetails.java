package wms.api.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "transaction_details")
public class TransactionDetails {

    @Id
    @Column(name = "bill_detail_id")
    private String billDetailId;

    @Column(name = "bill_detail")
    private String billDetail;

    private Integer quantity;

    @Column(name = "total_sales")
    private BigDecimal totalSales;

    @ManyToOne
    @JoinColumn(name = "bill_id", referencedColumnName = "bill_id")
    private Transactions transactions;

    @ManyToOne
    @JoinColumn(name = "product_id", referencedColumnName = "product_id")
    private Product product;

    @PrePersist
    public void prePersist() {
        if (this.billDetailId == null) this.billDetailId = UUID.randomUUID().toString();
        if (this.billDetail == null) this.billDetail = "default_bill_detail";
    }
}
