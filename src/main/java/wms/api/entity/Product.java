package wms.api.entity;

import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Data
@Builder
@Table(name = "products")
public class Product {

    @Id
    @Column(name = "product_id")
    private String productId;

    @Column(name = "product_price_id")
    private String productPriceId;

    @Column(name = "product_code")
    private String productCode;

    @Column(name = "product_name")
    private String productName;

    private BigDecimal price;

    @ManyToOne
    @JoinColumn(name = "branch_id", referencedColumnName = "branch_id")
    private Branch branch;

    @OneToMany(mappedBy = "product")
    private List<TransactionDetails> transactionDetails = new ArrayList<>();

    @PrePersist
    public void prePersist() {
        if (this.productId == null) this.productId = UUID.randomUUID().toString();
    }

//    @OneToMany(mappedBy = "product")
//    private List<Transactions> transactions;
}
