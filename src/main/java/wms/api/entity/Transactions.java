package wms.api.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "transactions")
public class Transactions {

    @Id
    @Column(name = "bill_id")
    private String billId;

    @Column(name = "receipt_number")
    private String receiptNumber;

    @Column(name = "trans_date")
    private LocalDateTime transDate;

    @Column(name = "transaction_type")
    private String transactionType;

//    @ManyToOne
//    @JoinColumn(name = "product_id", referencedColumnName = "product_id")
//    private Product product;

    @OneToMany(mappedBy = "transactions")
    private List<TransactionDetails> transactionDetails = new ArrayList<>();

    @PrePersist
    public void prePersist() {
        if (this.billId == null) this.billId = UUID.randomUUID().toString();
        if (this.receiptNumber == null) this.receiptNumber = "RN" + UUID.randomUUID().toString();
        if (this.transDate == null) this.transDate = LocalDateTime.now();
    }
}
