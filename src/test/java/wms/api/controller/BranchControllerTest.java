package wms.api.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import wms.api.entity.Branch;
import wms.api.model.BranchResponse;
import wms.api.model.CreateBranchRequest;
import wms.api.model.UpdateBranchRequest;
import wms.api.model.WebResponse;
import wms.api.repository.BranchRepository;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.MockMvcBuilder.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;

@SpringBootTest
@AutoConfigureMockMvc
class BranchControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private BranchRepository branchRepository;

    @BeforeEach
    void setUp() {
        branchRepository.deleteAll();
        objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
    }

    @Test
    void createBranchSuccess() throws Exception {
        CreateBranchRequest request = new CreateBranchRequest();
        request.setBranchName("Example");
        request.setBranchCode("12345");
        request.setAddress("Cibubur no 85, Rt 002 Rw 010, Jakarta timur, Jawa Barat");
        request.setPhoneNumber("089606891270");

        mockMvc.perform(
                post("/api/branch")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            String responseString = result.getResponse().getContentAsString();
            System.out.println("Response JSON: " + responseString);
            WebResponse<BranchResponse> response = objectMapper.readValue(responseString, new TypeReference<WebResponse<BranchResponse>>() {});
            assertNull(response.getErrors());
            assertNotNull(response.getData().getBranchId());
            assertEquals("Example", response.getData().getBranchName());
            assertEquals("12345", response.getData().getBranchCode());
            assertEquals("Cibubur no 85, Rt 002 Rw 010, Jakarta timur, Jawa Barat", response.getData().getAddress());
            assertEquals("089606891270", response.getData().getPhoneNumber());
        });
    }

    @Test
    void getBranch() throws Exception {
        Branch branch = new Branch();
        branch.setBranchId(UUID.randomUUID().toString());
        branch.setBranchName("test1");
        branch.setBranchCode("12345");
        branch.setAddress("Perum. Bumi Sindang Asri, B2 no 23, Cibarusah");
        branch.setPhoneNumber("089606891270");
        branchRepository.save(branch);

        mockMvc.perform(
                get("/api/branch/{id_branch}", branch.getBranchId())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            String responseString = result.getResponse().getContentAsString();
            System.out.println("Response JSON: " + responseString);
            WebResponse<BranchResponse> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });
            assertNull(response.getErrors());
            assertEquals(branch.getBranchId(), response.getData().getBranchId());
            assertEquals(branch.getBranchName(), response.getData().getBranchName());
            assertEquals(branch.getBranchCode(), response.getData().getBranchCode());
            assertEquals(branch.getAddress(), response.getData().getAddress());
            assertEquals(branch.getPhoneNumber(), response.getData().getPhoneNumber());

            assertNotNull(response.getPaging()); // Memastikan paging tidak null
            assertEquals(0, response.getPaging().getPage()); // Verifikasi nilai paging (sesuaikan dengan pengisian paging)
            assertEquals(10, response.getPaging().getSize());
            assertEquals(1, response.getPaging().getTotalItems());
            assertEquals(1, response.getPaging().getTotalPages());

        });
    }

    @Test
    void updateBranch() throws Exception {
        Branch branch = new Branch();
        branch.setBranchId(UUID.randomUUID().toString());
        branch.setBranchName("test2");
        branch.setBranchCode("123456");
        branch.setAddress("Perum. perum karawang jaya, Karawang, Jabar");
        branch.setPhoneNumber("089606891277");
        branchRepository.save(branch);

        UpdateBranchRequest request = new UpdateBranchRequest();
        request.setBranchId(UUID.randomUUID().toString());
        request.setBranchName("test3");
        request.setBranchCode("654321");
        request.setAddress("Cibubur, jakarta timur, DKI Jakarta");
        request.setPhoneNumber("0899778899");

        mockMvc.perform(
                put("/api/branch")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            String responseString = result.getResponse().getContentAsString();
            System.out.println("Response JSON: " + responseString);
            WebResponse<BranchResponse> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });

            assertNull(response.getErrors());
            assertEquals(request.getBranchId(), response.getData().getBranchId());
            assertEquals(request.getBranchName(), response.getData().getBranchName());
            assertEquals(request.getBranchCode(), response.getData().getBranchCode());
            assertEquals(request.getAddress(), response.getData().getAddress());
            assertEquals(request.getPhoneNumber(), response.getData().getPhoneNumber());
        });
    }

    @Test
    void deleteBranch() throws Exception {
        Branch branch = new Branch();
        branch.setBranchId(UUID.randomUUID().toString());
        branch.setBranchName("test5");
        branch.setBranchCode("1234567");
        branch.setAddress("Karawang, karawang barat jawa barat");
        branch.setPhoneNumber("0899775566");
        branchRepository.save(branch);

        mockMvc.perform(
                delete("/api/branch/{id_branch}", branch.getBranchId())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            String responseString = result.getResponse().getContentAsString();
            System.out.println("Response JSON: " + responseString);
            WebResponse<String> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });
            assertNull(response.getErrors());
            assertEquals("Ok", response.getData());
        });
    }
}