package wms.api.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import wms.api.entity.Branch;
import wms.api.entity.Product;
import wms.api.model.CreateProductRequest;
import wms.api.model.ProductResponse;
import wms.api.model.UpdateProductRequest;
import wms.api.model.WebResponse;
import wms.api.repository.BranchRepository;
import wms.api.repository.ProductRepository;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.MockMvcBuilder.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
class ProductControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private BranchRepository branchRepository;
    
    @Autowired
    private EntityManager entityManager; // kode baru

    private String branchId;

    @BeforeEach
    void setUp() {
        productRepository.deleteAll();
        branchRepository.deleteAll();
        objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);

        Branch branch = new Branch();
        branch.setBranchId(UUID.randomUUID().toString());
        branch.setBranchName("test1");
        branch.setBranchCode("12345");
        branch.setAddress("Cibubur, Jakarta - DKI Jakarta");
        branch.setPhoneNumber("089606891270");
        branchId = branch.getBranchId();

        // modif 2, periksa branch
//        Branch existingBranch = entityManager.find(Branch.class, branchId);
//        if (existingBranch == null) {
//            entityManager.persist(branch);
//        } else {
//            entityManager.merge(branch);
//        }
//        entityManager.flush();

        //branchRepository.save(branch); // Kode awal
        entityManager.persist(branch); // kode modif
        entityManager.flush(); // kode modif
//        entityManager.refresh(branch); // kode modif
    }

    @Test
    void createProduct() throws Exception {
        CreateProductRequest request = new CreateProductRequest();
        request.setProductName("Bakso");
        request.setProductCode("123456");
        request.setPrice(BigDecimal.valueOf(10000));
        request.setBranchId(branchId);

        mockMvc.perform(
                post("/api/products")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            String responseString = result.getResponse().getContentAsString();
            System.out.println("Response JSON: " + responseString);
            WebResponse<ProductResponse> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });
            assertNull(response.getErrors());
            assertEquals("Bakso", response.getData().getProductName());
            assertEquals("123456", response.getData().getProductCode());
            assertEquals(BigDecimal.valueOf(10000), response.getData().getPrice());
            assertNotNull(response.getData().getBranch());
            assertEquals(branchId, response.getData().getBranch().getBranchId());
        });
    }

    @Test
    void searchList() throws Exception {
        Branch branch =  new Branch();
        branch.setBranchId("branch-id");
        branch.setBranchName("branch-name");
        branch.setBranchCode("branch-code");
        branch.setAddress("branch-address");
        branch.setPhoneNumber("123456789");

        for (int i = 0; i < 20; i++) {
            Product product = new Product();
            product.setProductId(UUID.randomUUID().toString());
            product.setProductPriceId(UUID.randomUUID().toString());
            product.setProductName("test7 " + i);
            product.setProductCode("123456");
            product.setPrice(BigDecimal.valueOf(10000));
            product.setBranch(branch);

            entityManager.persist(product); // kode modif


        }
        entityManager.flush(); // kode modif

        mockMvc.perform(
                get("/api/products")
                        .param("branchId", "branch-id")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            String responseString = result.getResponse().getContentAsString();
            System.out.println("Response JSON: " + responseString);
            WebResponse<List<ProductResponse>> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });
            assertNull(response.getErrors());
            assertEquals(10, response.getData().size());
            assertEquals(2, response.getPaging().getTotalPages());
            assertEquals(10, response.getPaging().getSize());
        });
    }

    @Test
    void getProduct() throws Exception {
        Branch branch = new Branch();
        branch.setBranchId("branch-id");
        branch.setBranchName("branch-name");
        branch.setBranchCode("branch-code");
        branch.setAddress("branch-address");
        branch.setPhoneNumber("123456789");

        entityManager.persist(branch); // kode modif
        entityManager.flush(); // kode modif


        Product product = new Product();
        product.setProductId(UUID.randomUUID().toString());
        product.setProductPriceId(UUID.randomUUID().toString());
        product.setProductName("Sate");
        product.setProductCode("12345678");
        product.setPrice(BigDecimal.valueOf(20000));
        product.setBranch(branch);

        entityManager.persist(product); // kode modif
        entityManager.flush(); // kode modif


        mockMvc.perform(
                get("/api/products/" + branch.getBranchId())
                        .param("productId", product.getProductId())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            String responseString = result.getResponse().getContentAsString();
            System.out.println("Response JSON: " + responseString);
            WebResponse<ProductResponse> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {});
            assertNull(response.getErrors());
            assertEquals(product.getProductId(), response.getData().getProductId());
            assertEquals(product.getProductName(), response.getData().getProductName());
        });
    }

    @Test
    void update() throws Exception {
        // Membuat branch
        Branch branch = new Branch();
        branch.setBranchId(UUID.randomUUID().toString());
        branch.setBranchName("test branch 2");
        branch.setBranchCode("B002");
        branch.setAddress("Test Address 2");
        branch.setPhoneNumber("1234567890");

        // Menyimpan branch ke repository
        entityManager.persist(branch);
        entityManager.flush();

        // Membuat product
        Product product = new Product();
        product.setProductId(UUID.randomUUID().toString());
        product.setProductName("test before 2");
        product.setProductCode("543210");
        product.setProductPriceId(UUID.randomUUID().toString());
        product.setPrice(BigDecimal.valueOf(20000));
        product.setBranch(branch);

        // Menyimpan product ke repository
        entityManager.persist(product);
        entityManager.flush();

        // Membuat request untuk update product
        UpdateProductRequest request = new UpdateProductRequest();
        request.setProductId(product.getProductId());
        request.setProductName("test After 2");
        request.setProductCode("1234567");
        request.setPrice(BigDecimal.valueOf(17000));
        request.setProductPriceId(UUID.randomUUID().toString());
        request.setBranchId(branch.getBranchId());

        // Memastikan objectMapper mengkonversi objek ke JSON dengan benar
        String requestJson = objectMapper.writeValueAsString(request);

        mockMvc.perform(
                put("/api/products")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson)
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            String responseString = result.getResponse().getContentAsString();
            System.out.println("Response JSON: " + responseString);
            WebResponse<ProductResponse> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {});
            assertNull(response.getErrors());
            assertEquals(request.getProductId(), response.getData().getProductId());
            assertEquals(request.getProductName(), response.getData().getProductName());
            assertEquals(request.getProductCode(), response.getData().getProductCode());
            assertEquals(request.getPrice(), response.getData().getPrice());
            assertEquals(request.getProductPriceId(), response.getData().getProductPriceId());
            assertEquals(request.getBranchId(), response.getData().getBranch().getBranchId());
            assertEquals(branch.getBranchCode(), response.getData().getBranch().getBranchCode());
            assertEquals(branch.getBranchName(), response.getData().getBranch().getBranchName());
            assertEquals(branch.getAddress(), response.getData().getBranch().getAddress());
            assertEquals(branch.getPhoneNumber(), response.getData().getBranch().getPhoneNumber());
        });
    }

    @Test
    void deleteProduct() throws Exception {
        Branch branch = new Branch();
        branch.setBranchId(UUID.randomUUID().toString());
        branch.setBranchName("test delete");
        branch.setBranchCode("1234567");
        branch.setAddress("test delete product");
        branch.setPhoneNumber("0987654321");

        branchRepository.save(branch);

        Product product = new Product();
        product.setProductId(UUID.randomUUID().toString());
        product.setProductName("tester delete product");
        product.setProductCode("0123456789");
        product.setPrice(BigDecimal.valueOf(20000));
        product.setProductPriceId(UUID.randomUUID().toString());
        product.setBranch(branch);

        productRepository.save(product);

        mockMvc.perform(
                delete("/api/products/{id_product}", product.getProductId()) // Menambahkan path variable productId
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            String responseString = result.getResponse().getContentAsString();
            System.out.println("Response JSON: " + responseString);
            WebResponse<String> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });
            assertNull(response.getErrors());
            assertEquals("OK", response.getData());
        });
    }
}