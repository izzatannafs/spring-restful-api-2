package wms.api.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import wms.api.entity.Branch;
import wms.api.entity.Product;
import wms.api.model.CreateTransactionRequest;
import wms.api.model.TransactionResponse;
import wms.api.model.WebResponse;
import wms.api.repository.BranchRepository;
import wms.api.repository.ProductRepository;
import wms.api.repository.TransactionDetailsRepository;
import wms.api.repository.TransactionRepository;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.MockMvcBuilder.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;

@SpringBootTest
@AutoConfigureMockMvc
class TransactionControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private TransactionDetailsRepository transactionDetailsRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private BranchRepository branchRepository;

    @BeforeEach
    void setUp() {
        // Membersihkan data yang ada di database sebelum setiap test
        transactionDetailsRepository.deleteAll();
        transactionRepository.deleteAll();
        productRepository.deleteAll();
        branchRepository.deleteAll();

        objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);

//        // Membuat data branch
//        Branch branch = new Branch();
//        branch.setBranchId("branch123");
//        branch.setBranchCode("BR001");
//        branch.setBranchName("Main Branch");
//        branch.setAddress("123 Main Street");
//        branch.setPhoneNumber("123-456-7890");
//        branchRepository.save(branch);
//
//        // Membuat data produk
//        Product product1 = new Product();
//        product1.setProductId("prod123");
//        product1.setProductPriceId("12345");
//        product1.setProductCode("P001");
//        product1.setProductName("Product 1");
//        product1.setPrice(BigDecimal.valueOf(10000));
//        product1.setBranch(branch);
//        productRepository.save(product1);
//
//        Product product2 = new Product();
//        product2.setProductId("prod456");
//        product2.setProductPriceId("67890");
//        product2.setProductCode("P002");
//        product2.setProductName("Product 2");
//        product2.setPrice(BigDecimal.valueOf(15000));
//        product2.setBranch(branch);
//        productRepository.save(product2);
    }

    @Test
    void createTransactionTest() throws Exception {

        // Membuat permintaan transaksi
        CreateTransactionRequest request = CreateTransactionRequest.builder()
                .transactionType("1") // EAT IN
                .billDetail(Arrays.asList(
                        CreateTransactionRequest.BillDetailRequest.builder()
                                .productPriceId("12345")
                                .quantity(2)
                                .build(),
                        CreateTransactionRequest.BillDetailRequest.builder()
                                .productPriceId("67890")
                                .quantity(3)
                                .build()
                ))
                .build();

        // Konversi request ke JSON
        String requestJson = objectMapper.writeValueAsString(request);

        // Melakukan permintaan HTTP POST
        mockMvc.perform(
                        post("/api/transactions")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(requestJson)
                                .accept(MediaType.APPLICATION_JSON)
                ).andExpect(status().isOk())
                .andDo(result -> {
                    // Mengambil dan mencetak respons JSON
                    String responseString = result.getResponse().getContentAsString();
                    System.out.println("Response JSON: " + responseString);

                    // Memverifikasi respons JSON
                    WebResponse<TransactionResponse> response = objectMapper.readValue(responseString, new TypeReference<>() {});
                    assertNull(response.getErrors());

                    // Verifikasi data transaksi
                    TransactionResponse data = response.getData();
                    assertNotNull(data.getBillId());
                    assertNotNull(data.getReceiptNumber());
                    assertNotNull(data.getTransDate());
                    assertEquals("1", data.getTransactionType());

                    // Verifikasi detail tagihan
                    List<TransactionResponse.BillDetailResponse> billDetails = data.getBillDetails();
                    assertEquals(2, billDetails.size());

                    TransactionResponse.BillDetailResponse billDetail1 = billDetails.get(0);
                    assertNotNull(billDetail1.getBillDetailId());
                    assertEquals(data.getBillId(), billDetail1.getBillId());

                    TransactionResponse.BillDetailResponse.ProductResponse product1 = billDetail1.getProduct();
                    assertNotNull(product1.getProductId());
                    assertEquals("12345", product1.getProductPriceId());
                    assertEquals(2, billDetail1.getQuantity().intValue());
                    assertNotNull(billDetail1.getTotalSales());

                    TransactionResponse.BillDetailResponse.ProductResponse.BranchResponse branch1 = product1.getBranch();
                    assertNotNull(branch1.getBranchId());
                    assertNotNull(branch1.getBranchCode());
                    assertNotNull(branch1.getBranchName());
                    assertNotNull(branch1.getAddress());
                    assertNotNull(branch1.getPhoneNumber());

                    TransactionResponse.BillDetailResponse billDetail2 = billDetails.get(1);
                    assertNotNull(billDetail2.getBillDetailId());
                    assertEquals(data.getBillId(), billDetail2.getBillId());

                    TransactionResponse.BillDetailResponse.ProductResponse product2 = billDetail2.getProduct();
                    assertNotNull(product2.getProductId());
                    assertEquals("67890", product2.getProductPriceId());
                    assertEquals(3, billDetail2.getQuantity().intValue());
                    assertNotNull(billDetail2.getTotalSales());

                    TransactionResponse.BillDetailResponse.ProductResponse.BranchResponse branch2 = product2.getBranch();
                    assertNotNull(branch2.getBranchId());
                    assertNotNull(branch2.getBranchCode());
                    assertNotNull(branch2.getBranchName());
                    assertNotNull(branch2.getAddress());
                    assertNotNull(branch2.getPhoneNumber());
                });
    }

    @Test
    void createTrans() throws Exception {
        Branch branch = Branch.builder()
                .branchId("branch123")
                .branchCode("BR001")
                .branchName("Main Branch")
                .address("123 Main Street")
                .phoneNumber("123-456-7890")
                .build();
        branchRepository.save(branch);

        Product product1 = Product.builder()
                .productId("prod123")
                .productPriceId("12345")
                .productCode("P001")
                .productName("Product 1")
                .price(new BigDecimal(10000))
                .branch(branch)
                .build();
        productRepository.save(product1);

        Product product2 = Product.builder()
                .productId("prod456")
                .productPriceId("67890")
                .productCode("P002")
                .productName("Product 2")
                .price(new BigDecimal(15000))
                .branch(branch)
                .build();
        productRepository.save(product2);

        // Membuat permintaan transaksi
        CreateTransactionRequest request = CreateTransactionRequest.builder()
                .transactionType("1") // EAT IN
                .billDetail(Arrays.asList(
                        CreateTransactionRequest.BillDetailRequest.builder()
                                .productPriceId("12345")
                                .quantity(2)
                                .build(),
                        CreateTransactionRequest.BillDetailRequest.builder()
                                .productPriceId("67890")
                                .quantity(3)
                                .build()
                ))
                .build();

        // Konversi request ke JSON
        String requestJson = objectMapper.writeValueAsString(request);

        // Melakukan permintaan HTTP POST
        mockMvc.perform(
                        post("/api/transactions")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(requestJson)
                                .accept(MediaType.APPLICATION_JSON)
                ).andExpect(status().isOk())
                .andDo(result -> {
                    // Mengambil dan mencetak respons JSON
                    String responseString = result.getResponse().getContentAsString();
                    System.out.println("Response JSON: " + responseString);

                    // Memverifikasi respons JSON
                    WebResponse<TransactionResponse> response = objectMapper.readValue(responseString, new TypeReference<>() {});
                    assertNull(response.getErrors());

                    // Verifikasi data transaksi
                    TransactionResponse data = response.getData();
                    assertNotNull(data.getBillId());
                    assertNotNull(data.getReceiptNumber());
                    assertNotNull(data.getTransDate());
                    assertEquals("EAT_IN", data.getTransactionType());

                    // Verifikasi detail tagihan
                    List<TransactionResponse.BillDetailResponse> billDetails = data.getBillDetails();
                    assertEquals(2, billDetails.size());

                    TransactionResponse.BillDetailResponse billDetail1 = billDetails.get(0);
                    assertNotNull(billDetail1.getBillDetailId());
                    assertEquals(data.getBillId(), billDetail1.getBillId());

                    TransactionResponse.BillDetailResponse.ProductResponse product1Response = billDetail1.getProduct();
                    assertNotNull(product1Response.getProductId());
                    assertEquals("12345", product1Response.getProductPriceId());
                    assertEquals(2, billDetail1.getQuantity().intValue());
                    assertNotNull(billDetail1.getTotalSales());

                    TransactionResponse.BillDetailResponse.ProductResponse.BranchResponse branch1Response = product1Response.getBranch();
                    assertNotNull(branch1Response.getBranchId());
                    assertNotNull(branch1Response.getBranchCode());
                    assertNotNull(branch1Response.getBranchName());
                    assertNotNull(branch1Response.getAddress());
                    assertNotNull(branch1Response.getPhoneNumber());

                    TransactionResponse.BillDetailResponse billDetail2 = billDetails.get(1);
                    assertNotNull(billDetail2.getBillDetailId());
                    assertEquals(data.getBillId(), billDetail2.getBillId());

                    TransactionResponse.BillDetailResponse.ProductResponse product2Response = billDetail2.getProduct();
                    assertNotNull(product2Response.getProductId());
                    assertEquals("67890", product2Response.getProductPriceId());
                    assertEquals(3, billDetail2.getQuantity().intValue());
                    assertNotNull(billDetail2.getTotalSales());

                    TransactionResponse.BillDetailResponse.ProductResponse.BranchResponse branch2Response = product2Response.getBranch();
                    assertNotNull(branch2Response.getBranchId());
                    assertNotNull(branch2Response.getBranchCode());
                    assertNotNull(branch2Response.getBranchName());
                    assertNotNull(branch2Response.getAddress());
                    assertNotNull(branch2Response.getPhoneNumber());
                });
    }

    @Test
    void getTrans() throws Exception {
        // Mempersiapkan data untuk pengujian
        Branch branch = Branch.builder()
                .branchId("branch123")
                .branchCode("BR001")
                .branchName("Main Branch")
                .address("123 Main Street")
                .phoneNumber("123-456-7890")
                .build();
        branchRepository.save(branch);

        Product product = Product.builder()
                .productId("prod123")
                .productPriceId("12345")
                .productCode("P001")
                .productName("Product 1")
                .price(new BigDecimal(10000))
                .branch(branch)
                .build();
        productRepository.save(product);

        CreateTransactionRequest request = CreateTransactionRequest.builder()
                .transactionType("1") // EAT IN
                .billDetail(Arrays.asList(
                        CreateTransactionRequest.BillDetailRequest.builder()
                                .productPriceId("12345")
                                .quantity(2)
                                .build()
                ))
                .build();

        // Konversi request ke JSON
        String requestJson = objectMapper.writeValueAsString(request);

        // Melakukan permintaan HTTP POST untuk membuat transaksi
        MvcResult postResult = mockMvc.perform(
                        post("/api/transactions")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(requestJson)
                                .accept(MediaType.APPLICATION_JSON)
                ).andExpect(status().isOk())
                .andReturn();

        // Mengambil ID transaksi yang dibuat dari respons POST
        String postResponseString = postResult.getResponse().getContentAsString();
        WebResponse<TransactionResponse> postResponse = objectMapper.readValue(postResponseString, new TypeReference<>() {});
        String createdBillId = postResponse.getData().getBillId();

        // Melakukan permintaan HTTP GET untuk mengambil transaksi yang baru dibuat
        mockMvc.perform(
                        get("/api/transaction/{id_bill}", createdBillId)
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(status().isOk())
                .andDo(result -> {
                    // Mengambil dan mencetak respons JSON
                    String responseString = result.getResponse().getContentAsString();
                    System.out.println("Response JSON: " + responseString);

                    // Memverifikasi respons JSON
                    WebResponse<TransactionResponse> response = objectMapper.readValue(responseString, new TypeReference<>() {});
                    assertNull(response.getErrors());

                    // Verifikasi data transaksi
                    TransactionResponse data = response.getData();
                    assertNotNull(data.getBillId());
                    assertNotNull(data.getReceiptNumber());
                    assertNotNull(data.getTransDate());
                    assertEquals("EAT_IN", data.getTransactionType());

                    // Verifikasi detail tagihan
                    List<TransactionResponse.BillDetailResponse> billDetails = data.getBillDetails();
                    assertEquals(1, billDetails.size());

                    TransactionResponse.BillDetailResponse billDetail1 = billDetails.get(0);
                    assertNotNull(billDetail1.getBillDetailId());
                    assertEquals(data.getBillId(), billDetail1.getBillId());

                    TransactionResponse.BillDetailResponse.ProductResponse product1Response = billDetail1.getProduct();
                    assertNotNull(product1Response.getProductId());
                    assertEquals("P001", product1Response.getProductCode());
                    assertEquals("Product 1", product1Response.getProductName());
                    assertEquals(new BigDecimal(10000), product1Response.getPrice());
                    assertEquals(2, billDetail1.getQuantity().intValue());

                    TransactionResponse.BillDetailResponse.ProductResponse.BranchResponse branch1Response = product1Response.getBranch();
                    assertNotNull(branch1Response.getBranchId());
                    assertEquals("branch123", branch1Response.getBranchId());
                    assertEquals("BR001", branch1Response.getBranchCode());
                    assertEquals("Main Branch", branch1Response.getBranchName());
                    assertEquals("123 Main Street", branch1Response.getAddress());
                    assertEquals("123-456-7890", branch1Response.getPhoneNumber());
                });
    }



}